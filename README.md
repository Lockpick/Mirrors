# Mirrors

[git.zzls.xyz](https://git.zzls.xyz/NINTENSHIT)

[GitGud.io](https://gitgud.io/LP)

[Codeberg](https://codeberg.org/Lockpick)

[NotABug](https://notabug.org/LP)

[GitHub (No Binaries)](https://github.com/github/dmca/commit/f56aaad62eb8ca2f3dc471c13366c3ab5d46a1de)

[Disroot](https://git.disroot.org/Lockpick)

[Sanctuary](https://intosanctuary.com/index.php?resources/nintendo-switch-lockpick-and-lockpick-rcm-tools-and-source.13/)

[IPFS](https://cloudflare-ipfs.com/ipfs/QmcuXzqYozamN7PgZpzKT6qu6DRZyXgisuB2gx4aoGpG51)

[Wayback Machine (Binaries Only)](https://web.archive.org/web/20230507184216mp_/https://github.com/shchmue/Lockpick_RCM/releases/download/v1.9.10/Lockpick_RCM.bin), [2](https://web.archive.org/web/20230507213311mp_/https://github.com/shchmue/Lockpick/releases/download/v1.2.6/Lockpick.nro)